from setuptools import setup, Extension
from sequenza import __version__
import sys
import os

#os.environ["CC"] = "g++-5"
#os.environ["CXX"] = "g++-5"

install_requires = []

if sys.version_info < (2, 7):
    raise Exception('sequenza-utils requires Python 2.7 or higher.')

try:
    import argparse
except ImportError:
    install_requires.append('argparse')


setup(
    name='sequenza-utils',
    version=__version__.VERSION,
    description='Analysis of cancer sequencing, utilities for the R sequenza package.',
    long_description=open('README.txt').read(),
    author=__version__.AUTHOR,
    author_email=__version__.EMAIL,
    url=__version__.WEBSITE,
    license='LICENSE.txt',
    packages=['sequenza', 'sequenza.programs'],
    ext_modules=[Extension("sequenza.c_pileup", sources=[
        "sequenza/src/parsers.c", "sequenza/src/pileup.c"]),
        Extension("sequenza.c_seqz", sources=[
        "sequenza/src/parsers.c", "sequenza/src/seqz.c"])],
    test_suite='test',
    entry_points={
        'console_scripts': ['sequenza-utils = sequenza.commands:main']
    },
    install_requires=install_requires,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering',
    ],
    keywords='bioinformatics'
)
