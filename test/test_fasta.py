import unittest
from sequenza import fasta, misc


class TestFasta(unittest.TestCase):

    def test_fasta_read(self):
        with misc.xopen('test/data/test.fa.gz', 'rt') as fa:
            test = fasta.Fasta(fa, 10)
            for line in test:
                if line:
                    self.assertEqual(len(line), 4)
                last = line
            self.assertEqual(last, ('t3', 351, 360, 'AAAAAAAAAA'))

if __name__ == '__main__':
    unittest.main()
