import unittest
import os
import stat
from sequenza import misc


class TestMiscs(unittest.TestCase):

    def test_xopen(self):
        file = "test/data/1.gc.gz"
        with misc.xopen(file, "rb") as gc:
            head = next(gc).strip()
        self.assertEqual(head, b'variableStep chrom=1 span=50')

    # def test_IterableQueue(self):
    #    testq = misc.IterableQueue()
    #    self.assertTrue(testq.empty())
    #    for n in xrange(0, 100):
    #       testq.put(n)
    #    for m in xrange(0, 100):
    #       self.assertTrue(testq.get() == m)
    #    self.assertTrue(testq.empty())

    def test_countN(self):
        self.assertEqual(misc.countN('CCCCC', 'C'), 100)
        self.assertEqual(misc.countN('CCCCC', 'G'), 0)
        seq = 'AAACCCGGG'
        self.assertEqual(
            int(misc.countN(seq, 'C') + misc.countN(seq, 'G')), 66)

    # def test_grouper(self):
    #   groups = misc.grouper(3, "abcdefg", "x")
    #   self.assertEqual(groups.next(), ('a', 'b', 'c'))
    #   self.assertEqual(groups.next(), ('d', 'e', 'f'))
    #   self.assertEqual(groups.next(), ('g', 'x', 'x'))

if __name__ == '__main__':
    unittest.main()
