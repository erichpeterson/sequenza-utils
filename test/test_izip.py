import unittest
from sequenza import wig, misc, izip
try:
    from cStringIO import StringIO
except:
    try:
        from StringIO import StringIO
    except:
        from io import StringIO


class TestIzip(unittest.TestCase):

    def test_coordinate_zip(self):
        with misc.xopen('test/data/normal.pileup.gz', 'rt') as normal, misc.xopen('test/data/tumor.pileup.gz', 'rt') as tumor, misc.xopen('test/data/1.gc.gz', 'rt') as gc:
            nor_tum = izip.zip_coordinates(misc.split_coordinates(
                normal), misc.split_coordinates(tumor))
            nor_tum_gc = izip.zip_coordinates(nor_tum, wig.Wiggle(gc))
            res = next(nor_tum_gc)
            self.assertEqual(res[1][2], '38.0')
            self.assertEqual(res[1][0].split('\t')[0],
                             res[1][1].split('\t')[0])
            self.assertEqual(res[0][1], 95369073)

if __name__ == '__main__':
    unittest.main()
