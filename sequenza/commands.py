#!/usr/bin/env python
# -*- coding: utf-8 -*-


from sequenza.misc import get_modules, DefaultHelpParser, SubcommandHelpFormatter
from sequenza import __version__
import sequenza.programs


def main():
    '''
    Execute the function with args
    '''
    parser = DefaultHelpParser(prog='sequenza-utils', formatter_class=lambda prog: SubcommandHelpFormatter(prog, max_help_position=20, width=75),
                               description='Sequenza Utils is an ensemble of tools capable of perform various tasks, primarily aimed to convert bam/pileup files to a format usable by the sequenza R package',
                               epilog='This is version %s - %s - %s' % (__version__.VERSION, __version__.AUTHOR, __version__.DATE))
    subparsers = parser.add_subparsers(dest='module')

    modules = get_modules(sequenza.programs, subparsers, {})
    try:
        args, extra = parser.parse_known_args()
        if args.module in modules.keys():
            modules[args.module](parser, subparsers, args.module)
        else:
            return parser.parse_args(extra)
    except IndexError:
        return parser.print_help()

if __name__ == "__main__":
    main()
