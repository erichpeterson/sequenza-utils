from sequenza.misc import xopen, countN
from sequenza.seqz import binned_seqz


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name, add_help=False,
                                 help='Perform the binning of the seqz file to reduce file size \
                                        and memory requirement for the analysis.')


def binning_args(parser, subparser):
    subparser.add_argument('-s', '--seqz', dest='seqz', required=True,
                           help='An seqz file from the pileup2seqz function.')
    subparser.add_argument('-w', '--window', dest='window', type=int, default=50,
                           help='Window size used for binning the original seqz file. Default is 50.')
    subparser.add_argument('-o', dest='out', type=str, default='-',
                           help='Output file \"-\" for STDOUT')
    return parser.parse_args()
    return parser.parse_args()


def seqz_binning(parser, subparsers, module_name):
    args = binning_args(parser, add_parser(subparsers, module_name))
    with xopen(args.seqz, 'rt') as seqz, xopen(args.out, 'wt') as out:
        bins = binned_seqz(seqz, args.window)
        out.write(bins._header + '\n')
        for block in bins:
            if block:
                out.write('\t'.join(block['top']) + '\n')
                for mid in block['middle']:
                    out.write('\t'.join(mid) + '\n')
                if ['bottom'] != ['top']:
                    out.write('\t'.join(block['bottom']) + '\n')
