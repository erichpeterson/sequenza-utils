from sequenza.misc import xopen, split_coordinates
from sequenza.pileup import acgt
import sys

try:
    import __pypy__
    from sequenza.pileup import acgt
except ImportError:
    try:
        from sequenza.c_pileup import acgt
    except ImportError:
        from sequenza.pileup import acgt


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name, add_help=False,
                                 help='Parse the format from the samtools mpileup command, \
                                        and report the occurrence of the 4 nucleotides in each position.')


def pileup2actg_args(parser, subparser):
    parser_io = subparser.add_argument_group(
        title='Output', description='Arguments that involve the output destination.')
    parser_io.add_argument(
        '-p', '--mpileup', dest='mpileup', required=True,
        help='Name of the input mpileup (SAMtools) file. If the filename ends in .gz it will \
                                  be opened in gzip mode. If the file name is - it will be read from STDIN.')
    parser_io.add_argument(
        '-o', '--output', dest='output', default='-',
        help='Name of the output file. To use gzip compression name the file ending in .gz. Default STDOUT.')
    parser_filters = subparser.add_argument_group(
        title='Filtering and Format', description='Arguments that apply some filter to process the mpileup.')
    parser_filters.add_argument(
        '-n', dest='n', default=1, type=int,
        help='The minimum read depth on a base required to make a mutation call.')
    parser_filters.add_argument(
        '-q', '--qlimit', dest='qlimit', default=20, type=int,
        help='Minimum nucleotide quality score for inclusion in the counts.')
    parser_filters.add_argument(
        '--no-end', dest='noend', action='store_true',
        help='Discard the base located at the end of the read')
    parser_filters.add_argument(
        '--no-start', dest='nostart', action='store_true',
        help='Discard the base located at the start of the read')
    parser_filters.add_argument(
        '-f', '--qformat', dest='qformat', default="sanger",
        help='Quality format, options are "sanger" or "illumina". This will add an offset of 33 or 64 respectively to the qlimit value.')

    return parser.parse_args()


def pileup2acgt(parser, subparsers, module_name):
    args = pileup2actg_args(parser, add_parser(subparsers, module_name))
    header = ['chr', 'n_base', 'ref_base',
              'read.depth', 'A', 'C', 'G', 'T', 'strand']
    qlimit = args.qlimit
    if args.qformat == 'sanger':
        qlimit = qlimit + 33
    elif args.qformat == 'illumina':
        qlimit = qlimit + 64
    else:
        sys.exit(
            "Supported quality format are only \"illumina\" and \"sanger\"(default).")

    with xopen(args.mpileup, 'rt') as mpileup, xopen(args.output, 'wt') as out:
        out.write('\t'.join(map(str, header)) + '\n')
        for line in mpileup:
            try:
                chromosome, position, reference, depth, pileup, quality = line.strip().split('\t')
                depth = int(depth)
                reference = reference.upper()
                if depth >= args.n and args.n > 0 and reference != 'N':
                    acgt_res = acgt(pileup, quality, int(depth), reference, qlimit=qlimit,
                                    noend=args.noend, nostart=args.nostart)
                    #line = [chromosome, position, reference, depth, acgt['A'], acgt['C'], acgt['G'], acgt['T'], ':'.join(map(str, acgt['Z']))]
                    #out.write('\t'.join(map(str, line)) + '\n')
                    out.write(chromosome + '\t' + str(position) + '\t' + reference + '\t' + str(depth) + '\t' + str(acgt_res['A']) + '\t' + str(acgt_res['C']) + '\t' + str(acgt_res[
                              'G']) + '\t' + str(acgt_res['T']) + '\t' + str(acgt_res['Z'][0]) + ':' + str(acgt_res['Z'][1]) + ':' + str(acgt_res['Z'][2]) + ':' + str(acgt_res['Z'][3]) + '\n')
            except ValueError:
                pass
