try:
    import __pypy__
    from sequenza.pileup import acgt
except ImportError:
    try:
        from sequenza.c_pileup import acgt
    except ImportError:
        from sequenza.pileup import acgt


def process_merged_data(data, depth_sum=20, qlimit=53, hom_t=0.85, het_t=0.35, het_f=-0.1):
    '''
    After the 3 files are synchronized we need to transform the pileup in a
    readable format, find the alleles, and compute the allele frequency in the tumor
    '''
    if data:
        bases_list = ['A', 'C', 'G', 'T']
        if len(data) == 4:
            alt_pileup = True
            #chromosome, position, ref, p1_str, p2_str, gc, alt_depth = data
            normal_line, tumor_line, gc, alt_line = data
            try:
                null, alt_depth, null = alt_line.split('\t', 2)
            except ValueError:
                return None
            alt_depth = int(alt_depth)

        else:
            alt_pileup = False
            #chromosome, position, ref, p1_str, p2_str, gc = line
            normal_line, tumor_line, gc = data

        ref, p1_str = normal_line.split('\t', 1)
        null, p2_str = tumor_line.split('\t', 1)

        p1_list = p1_str.split()
        p2_list = p2_str.split()
        ref = str(ref).upper()
        if int(p1_list[0]) + int(p2_list[0]) >= depth_sum and len(p1_list) == len(p2_list):
            p1_mu = acgt(p1_list[1], p1_list[2], int(p1_list[0]), ref, qlimit)
            sum_p1 = float(
                sum({k: p1_mu[k] for k in ('A', 'C', 'G', 'T')}.values()))
            if sum_p1 > 0:
                p1_freq = [x / sum_p1 for x in [p1_mu['A'],
                                                p1_mu['C'], p1_mu['G'], p1_mu['T']]]
                sort_freq_p1 = sorted(p1_freq, reverse=True)
                if sort_freq_p1[0] >= hom_t:
                    # Homozygous positions here
                    i = p1_freq.index(sort_freq_p1[0])
                    p2_mu = acgt(p2_list[1], p2_list[2],
                                 int(p2_list[0]), ref, qlimit)
                    sum_p2 = float(
                        sum({k: p2_mu[k] for k in ('A', 'C', 'G', 'T')}.values()))
                    if sum_p2 > 0:
                        p2_freq = [x / sum_p2 for x in [p2_mu['A'],
                                                        p2_mu['C'], p2_mu['G'], p2_mu['T']]]
                        no_zero_idx = [val for val in range(
                            len(p2_freq)) if p2_freq[val] > 0]
                        no_zero_bases = [
                            str(bases_list[ll]) + str(round(p2_freq[ll], 3)) for ll in no_zero_idx if ll != i]
                        if no_zero_bases == []:
                            no_zero_bases = '.'
                            strands_bases = '0'
                        else:
                            no_zero_bases = ":".join(map(str, no_zero_bases))
                            strands_bases = [str(bases_list[ll]) + str(round(p2_mu['Z'][ll] / float(
                                p2_mu[bases_list[ll]]), 3)) for ll in no_zero_idx if ll != i]
                            strands_bases = ":".join(map(str, strands_bases))
                        homoz_p2 = p2_mu[bases_list[i]] / sum_p2
                        # base_ref, depth_normal, depth_sample, depth.ratio,
                        # Af, Bf, zygosity.normal, GC-content, reads above
                        # quality, AB.ref, AB.tum, percentage AB.tum in fw
                        if alt_pileup:
                            line_out = [ref, alt_depth, p2_list[0], round(int(p2_list[0]) / float(alt_depth), 3), round(
                                homoz_p2, 3), 0, 'hom', gc, int(sum_p2), bases_list[i], no_zero_bases, strands_bases]
                        else:
                            line_out = [ref, p1_list[0], p2_list[0], round(int(p2_list[0]) / float(p1_list[0]), 3), round(
                                homoz_p2, 3), 0, 'hom', gc, int(sum_p2), bases_list[i], no_zero_bases, strands_bases]
                        return line_out
                    else:
                        pass
                else:
                    if sort_freq_p1[1] >= het_t:
                        # Heterozygous position here
                        allele = list()
                        for b in range(4):
                            if p1_freq[b] >= het_t:
                                allele.append(bases_list[b])
                        if len(allele) == 2:
                            het_f = [het_f, 1 - het_f]
                            strands_bases = [round(p1_mu['Z'][
                                                   ll] / float(p1_mu[bases_list[ll]]), 3) for ll in map(bases_list.index, allele)]
                            if het_f[0] < strands_bases[0] < het_f[1] and het_f[0] < strands_bases[1] < het_f[1]:
                                #   print "Heterozygous strands " + ':'.join(map(str, strands_bases)) + " " +':'.join(map(str, het_f))
                                # else:
                                # print "Heterozygous no good " +
                                # ':'.join(map(str, strands_bases))  + " "
                                # +':'.join(map(str, het_f))
                                p2_mu = acgt(p2_list[1], p2_list[2], int(
                                    p2_list[0]), ref, qlimit)
                                sum_p2 = float(
                                    sum({k: p2_mu[k] for k in ('A', 'C', 'G', 'T')}.values()))
                                if sum_p2 > 0:
                                    i = bases_list.index(allele[0])
                                    ii = bases_list.index(allele[1])
                                    het_a_p2 = p2_mu[bases_list[i]] / sum_p2
                                    het_b_p2 = p2_mu[bases_list[ii]] / sum_p2
                                    if het_a_p2 >= het_b_p2:
                                        if alt_pileup:
                                            line_out = [ref, alt_depth, p2_list[0], round(int(p2_list[0]) / float(alt_depth), 3), round(
                                                het_a_p2, 3), round(het_b_p2, 3), 'het', gc, int(sum_p2), bases_list[i] + bases_list[ii], ".", "0"]
                                        else:
                                            line_out = [ref, p1_list[0], p2_list[0], round(int(p2_list[0]) / float(p1_list[0]), 3), round(
                                                het_a_p2, 3), round(het_b_p2, 3), 'het', gc, int(sum_p2), bases_list[i] + bases_list[ii], ".", "0"]
                                        return line_out
                                    elif het_a_p2 < het_b_p2:
                                        if alt_pileup:
                                            line_out = [ref, alt_depth, p2_list[0], round(int(p2_list[0]) / float(alt_depth), 3), round(
                                                het_b_p2, 3), round(het_a_p2, 3), 'het', gc, int(sum_p2), bases_list[ii] + bases_list[i], ".", "0"]
                                        else:
                                            line_out = [ref, p1_list[0], p2_list[0], round(int(p2_list[0]) / float(p1_list[0]), 3), round(
                                                het_b_p2, 3), round(het_a_p2, 3), 'het', gc, int(sum_p2), bases_list[ii] + bases_list[i], ".", "0"]
                                        return line_out
                            else:
                                pass
                        else:
                            pass
            else:
                pass
        else:
            pass
    else:
        pass


class binned_seqz:

    def __init__(self, seqz, window):
        self.seqz = seqz
        self.window = window
        self._status = 1
        self._header = next(self.seqz).strip()
        line = next(self.seqz).strip()
        line_ls = line.split('\t')
        self.__refresh__(line_ls, line)
    _sentinel = object()

    def next(self):
        if self._status == 1:
            try:
                line = next(self.seqz).strip()
                line_ls = line.split('\t')
            except StopIteration:
                self._status = 0
                self.__do_dict__()
                # return [line_ls[1], self.window + self._last_edge, self._n]
                return self.line_dict
            if self.window + self._last_edge < int(line_ls[1]) or self._last_chromosome != line_ls[0]:
                self.__do_dict__()
                line_dict = self.line_dict
                self.__refresh__(line_ls, line)
                # return [line_ls[1], self.window + self._last_edge, self._n]
                return line_dict
            else:
                self.__addline__(line_ls, line)
        elif self._status == 0:
            raise StopIteration

    def __refresh__(self, line_ls, line):
        self._last_chromosome = line_ls[0]
        self._last_edge = int(line_ls[1])
        self._last_position = self._last_edge
        self._n_depth = int(line_ls[3])
        self._t_depth = int(line_ls[4])
        self._ratio = float(line_ls[5])
        self._gc = float(line_ls[9])
        self._n = 1
        self.line_dict = {'top': None, 'middle': [], 'bottom': None}
        if line_ls[12] + line_ls[8] != '.hom':
            self.line_dict['top'] = line_ls

    def __addline__(self, line_ls, line):
        self._last_position = int(line_ls[1])
        self._n_depth += int(line_ls[3])
        self._t_depth += int(line_ls[4])
        self._ratio += float(line_ls[5])
        self._gc += float(line_ls[9])
        self._n += 1
        if line_ls[12] + line_ls[8] != '.hom':
            self.line_dict['middle'].append(line_ls)

    def __do_dict__(self):
        gc = str(int(round(self._gc / self._n, 0)))
        avg_line = [self._last_chromosome, self._last_position, 'N', self._n_depth / self._n,
                    self._t_depth /
                    self._n, round(self._ratio / self._n, 3), 1.0, 0, 'hom',
                    gc, self._n, 'N', '.', '0']
        self.line_dict['bottom'] = map(str, avg_line)
        if self.line_dict['top'] is None:
            avg_line[1] = self._last_edge
            self.line_dict['top'] = map(str, avg_line)
        else:
            self.line_dict['top'][9] = gc
        for mid in self.line_dict['middle']:
            mid[9] = gc

    def close(self):
        self.seqz.close()

    def __iter__(self):
        return (iter(self.next, self._sentinel))
