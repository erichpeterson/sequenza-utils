import shlex
import subprocess


def bam_mpileup(bam, fasta, q=20, Q=20, samtools_bin='samtools', regions=[]):
    samtools_view = "%s view -u %s" % (samtools_bin, bam)
    samtools_view = shlex.split(samtools_view) + regions
    samtools_pile = "%s mpileup -f %s -q %i -Q %i -" % (
        samtools_bin, fasta, q, Q)
    samtools_pile = shlex.split(samtools_pile)
    proc1 = subprocess.Popen(
        samtools_view, stdout=subprocess.PIPE, bufsize=4096)
    proc2 = subprocess.Popen(samtools_pile, stdin=proc1.stdout,
                             stdout=subprocess.PIPE, bufsize=4096)
    for line in proc2.stdout:
        yield line.decode('utf-8')


def indexed_pileup(pileup, tabix_bin='tabix', regions=[]):
    tabix = "%s %s" % (tabix_bin, pileup)
    tabix = shlex.split(tabix) + regions
    proc1 = subprocess.Popen(tabix, stdout=subprocess.PIPE, bufsize=4096)
    for line in proc1.stdout:
        yield line.decode('utf-8')


def program_version(program):
    '''
    Parse the tabix os samtools help message in attempt to
    return the software version in a list [major, minor, *]
    '''
    proc1 = subprocess.Popen([program], stderr=subprocess.PIPE)
    for line in proc1.stderr:
        if line.startswith(b'Version:'):
            return map(int, line.rstrip().split(b' ')[1].split(b'.'))
