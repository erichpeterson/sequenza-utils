API interface
=============


sequenza.wig
--------------

.. automodule:: sequenza.wig
   :members:


sequenza.fasta
--------------

.. automodule:: sequenza.fasta
   :members:
