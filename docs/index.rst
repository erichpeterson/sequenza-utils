Sequenza-utils
==============

Sequenza-utils is a collection of utilities intended to assist the user on the analysis of tumor genomes with the `sequenza`_ R package.

The library provide a command line interface with several programs and a python API to enable further development and integration in other libraries.

User documentation
------------------

.. toctree::
   :maxdepth: 2

   install
   guide
   commands


API reference
-------------

.. toctree::
   :maxdepth: 2

   api

.. _sequenza: https://cran.r-project.org/web/packages/sequenza/index.html
