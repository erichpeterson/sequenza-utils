Installation
============

The sequenza-utils code is hosted in `BitBucket`_.

Supported Python version are 2.7+ and pypy.
Python 3+ might work, but for the time being the port is not fully tested.

Sequenza-utils can be installed either via the Python Package Index (PyPI)
or from the git repository.


Latest release via PyPI
-----------------------

To install the latest release via PyPI using pip::

    pip install sequenza-utils


Development version
-------------------

Using the latest development version directly from the git repository::

    git clone https://bitbucket.org/ffavero/sequenza-utils
    cd sequenza-utils
    python setup.py test 
    python setup.py install

.. _`BitBucket`: https://bitbucket.org/ffavero/sequenza-utils