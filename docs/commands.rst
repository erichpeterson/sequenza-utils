Command line interface
======================

The main sequenza-utils interface are expose through
various programs in the sequenza-utils exceutable.
::

    francesco@FFA:~$ usage: sequenza-utils [-h] {gc_wiggle} ...

    Sequenza Utils is an ensemble of tools capable of perform various tasks, primarily aimed to convert bam/pileup files to a format usable by the sequenza R package

    positional arguments:
        gc_wiggle  Given a fasta file and a window size it computes the GC
                   percentage across the sequences, and returns a file in the
                   same format as gc5Base from UCSC

    optional arguments:
      -h, --help   show this help message and exit

    This is version 2.2.0 - Francesco Favero - 3 March 2016


Each program is accessible in a similar manner:
::

    francesco@FFA:~$ sequenza-utils gc_windows -h
    usage: sequenza-utils gc_wiggle [-h] [-o OUT] [-w WINDOW] fasta

    positional arguments:
      fasta       the fasta file. It can be a file name or "-" to take the input
                  from STDIN

    optional arguments:
      -h, --help  show this help message and exit
      -o OUT      Output file "-" for STDOUT
      -w WINDOW   The window size to calculate the GC-content percentage
